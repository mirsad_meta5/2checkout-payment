<?php

require_once 'lib/Twocheckout.php';
$config = include_once './config.php';

Twocheckout::privateKey($config['privateKey']); //Private Key
Twocheckout::sellerId($config['sellerId']); // 2Checkout Account Number
Twocheckout::sandbox($config['sandbox']); // Set to false for production accounts.
Twocheckout::verifySSL($config['ssl']);

header("Content-Type: application/json; charset=UTF-8");

try {

    $amount = floatval($_POST['amount']);
    $currency = isset($_POST['currency']) ? $_POST['currency'] : $config['currency'];
    $billingAddrOverride = [];

    if (isset($_POST['holder'])) {
        $billingAddrOverride['name'] = trim($_POST['holder']);
    }

    $charge = Twocheckout_Charge::auth(
        [
            'merchantOrderId' => time() % 100000,
            'token' => $_POST['token'],
            'currency' => $currency,
            'total' => $amount,
            'lang' => 'bh',
            'billingAddr' => array_merge($config['contact'], $billingAddrOverride),
        ]
    );

    $wasSuccessful = 'APPROVED' == $charge['response']['responseCode'];
    $type = $wasSuccessful ? 'succes' : 'failure';
    $message = $wasSuccessful ? 'Payment sucessful!' : 'Unable to process payment!';

    echo json_encode(
        [
            $type => [
                'message' => $message,
                'checkout' => $charge,
            ],
        ]
    );
} catch (Twocheckout_Error $e) {
    echo json_encode(
        [
            'error' => [
                'message' => $e->getMessage(),
                'file' => $e->getFile(),
                'line' => $e->getLine(),
                'code' => $e->getCode(),
            ],
        ]
    );
}
