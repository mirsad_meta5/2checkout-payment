<?php

return [
    'publicKey' => '417904B9-3C29-404D-B0F0-662F6181BD55',
    'privateKey' => 'DE4D6D92-D6DE-47AE-BB0C-6B430222BDBA',
    'sellerId' => '901417416',
    'mode' => 'sandbox',
    'sandbox' => true,
    'ssl' => false,
    'currency' => 'BAM',
    'demo' => 'Y',
    'contact' => [
        'name' => 'Lets Go',
        'addrLine1' => 'Cazin bb',
        'city' => 'Cazin',
        'state' => 'FBiH',
        'zipCode' => '88000',
        'country' => 'BiH',
        'email' => 'salazar@email5.net',
        'phoneNumber' => '0000000',
    ]
];