<?php
require_once("lib/Twocheckout.php");

Twocheckout::privateKey('42D7EEAE-5D05-4CB8-B0BF-3234FFDF53EA'); //Private Key
Twocheckout::sellerId('901417398'); // 2Checkout Account Number
Twocheckout::sandbox(true); // Set to false for production accounts.
Twocheckout::verifySSL(false);

try {
    $charge = Twocheckout_Charge::auth(array(
        "merchantOrderId" => "123",
        "token"      => $_POST['token'],
        "currency"   => 'USD',
		"total" => '1.00',
		"lang"  => 'es',
		// "lineItems" => array (
			// "0" => array(
				// "type" => "product",
				// "name" => "test recurring",
				// "quantity" => "1",
				// "price" => "1.00",
				// "recurrence" => "1 Week",
				// "duration" => "Forever"
			// )
		// ),
        "billingAddr" => array(
            "name" => 'John Doe',
            "addrLine1" => 'Test',
            "city" => 'Bucharest',
            "state" => 'Georgia',
            "zipCode" => '10001',
            "country" => 'US',
            "email" => 'test@2checkout.com',
            "phoneNumber" => '0000000'
        )
    ));

    if ($charge['response']['responseCode'] == 'APPROVED') {
        echo "Thanks for your Order!";
        echo "<h3>Return Parameters:</h3>";
        echo "<pre>";
        print_r($charge);
        echo "</pre>";

    }
} catch (Twocheckout_Error $e) {
    print_r($_POST['token']);
    print_r($e->getMessage());
}