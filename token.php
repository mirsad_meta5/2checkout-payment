<?php $config = require_once './config.php'; ?>
<?php
    $decoded = base64_decode($_GET['data']);
    $data = json_decode($decoded, true);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <script type="text/javascript" src="https://www.2checkout.com/checkout/api/2co.min.js"></script>
</head>
<body>
    <h1 id="token"></h1>

 
</body>

<script>
    var successCallback = function(data) {
        document.querySelector('#token').innerHTML = data.response.token.token;
    };

    var errorCallback = function(data) {
        if (data.errorCode === 200) {
            tokenRequest();
        } else {
            alert(data.errorMsg);
            console.log(data);
        }
    };

    window.addEventListener('load', () => {
        TCO.loadPubKey("<?php echo $config['mode'] ?>", () => {
            var args = {
                sellerId: "<?php echo $config['sellerId']; ?>",
                publishableKey: "<?php echo $config['publicKey']; ?>",
                ccNo: "<?php echo $data['cc']; ?>",
                cvv: "<?php echo $data['cvv']; ?>",
                expMonth: "<?php echo $data['expMonth']; ?>",
                expYear: "<?php echo $data['expYear']; ?>",
            };

            TCO.requestToken(successCallback, errorCallback, args);
        });
    });

    
</script>   
</html>